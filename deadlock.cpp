// COMPILE CODE BY USING THIS COMMAND
// g++ deadlock.cpp -lpthread

#include <iostream>
#include <thread>
#include <mutex>
#include <string>

using namespace std;

mutex mu;
mutex mu2; 

void display(string msg)
{
    lock_guard<mutex> locker1(mu);
    cout << msg << endl;
    while(true){}
}

void display2(string msg)
{
    lock_guard<mutex> locker2(mu2);
    cout << msg << endl;
    while(true){}

}


void function1()
{    
    display("Thread 1 Start ");
    display2("Thread 1 Finished ");
    
}

void function2()
{
    display2("Thread 2 Start ");
    display("Thread 2 Finished");
}

int main()
{
   thread t1(function1);
   thread t2(function2);
   t1.join();
   t2.join();
   return 0;
}

